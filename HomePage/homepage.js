var user = localStorage.getItem("idUser");
$("document").ready(function (){

    $("h1.mensaje").text("Bienvenido "+user);

    $(".logout").on("click",function(){
        localStorage.clear();
    });
    
    $("#vg").click( function(){

        localStorage.setItem("id", "vg");
    } );
    $("#l").click( function(){

        localStorage.setItem("id", "l");
    } );
    $("#p").click(function(){
        
        localStorage.setItem("id", "p");
    } );

    $("img").click(function (){
        var itemId = $(this).attr("class");
        localStorage.setItem("itemId",itemId);
    })
})