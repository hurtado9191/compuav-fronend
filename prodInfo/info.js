var id = localStorage.getItem("itemId");
var link = localStorage.getItem("imagen");
let carrito = [];
$("document").ready(function(){
    if(localStorage.getItem("carrito") != null){
        carrito = JSON.parse(localStorage.getItem("carrito"));
    }

    $(".logout").on("click",function(){
        localStorage.clear();
    });

    $.ajax({
        'url': 'http://localhost:8080/item?id='+id,
        'type':'GET',
        'Access-Control-Allow-Origin': 'http://localhost:8080',
        'Access-Control-Allow-Methods': 'GET',
        'headers': {
            'Accept':'application/json',
            'Content-Type': 'application/json'},
            'responseType':'application/json',
            
    }).then(function(json){
        var item = json;
        var name= json.name;
        var costo = json.precio;
        $("#item").text(name);
        $("#precio").text(costo);
        $("img.item").attr("scr",link);
        $("#buyBtn").click(function(){
            carrito.push(item);
            localStorage.setItem("carrito", JSON.stringify(carrito));
        });
    });

});