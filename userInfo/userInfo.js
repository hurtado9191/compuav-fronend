var userId = localStorage.getItem("idUser");

$("document").ready(function (){

    $(".logout").on("click",function(){
        localStorage.clear();
    });
    
    $.ajax({
        'url': 'http://localhost:8080/misOrdenes' ,
        'type':'GET',
        'Access-Control-Allow-Origin': 'http://localhost:8080',
        'Access-Control-Allow-Methods': 'GET',
        'headers': {
            'Accept':'application/json',
            'Content-Type': 'application/json'},
            'responseType':'application/json',
    }).then(function(json){
        console.log(json);
    $.each(json, function(index, orden){
        console.log(json);
        createOrdenSpace(index);
        $.each(orden, function(index, item){
            console.log('primera llamada');
            $.each(item, function(index, items){
                console.log(items);
                createOrden(items);

            })
        })
    })
    });

    $.ajax({
        'url': 'http://localhost:8080/getUser?name='+ userId,
    'type':'GET',
    'Access-Control-Allow-Origin': 'http://localhost:8080',
    'Access-Control-Allow-Methods': 'GET',
    'headers': {
        'Accept':'application/json',
        'Content-Type': 'application/json'},
        'responseType':'application/json',
    }).then(function (json){
        console.log('Segunda llamada');
        $("#nombre").attr("value", json.user);
        $("#email").attr("value",json.email);
        $("#direccion").attr("value",json.direccion);
        $("#tarjeta").attr("value",json.tarjeta);
        $("#password").attr("value",json.pwd);
    })

    function createOrden(json){
        var string = JSON.stringify(json);
        var orden =  '<span class = "placeHolder"> <p>'+string+'<p></span>';
        $("div.lugar").append(orden);
    }
    function createOrdenSpace(index){
        var lugar = index+1;
        var cuadro = '<div class="lugar">Orden '+lugar+'</div>';
        $("div.orden").append(cuadro);
    }
});