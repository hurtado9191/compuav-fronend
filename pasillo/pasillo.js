var id = localStorage.getItem("id");
$("document").ready(function (){
    const selectorsP = ["p.first#precio", "p.second#precio", "p.third#precio"];
    const selectorsI = ["p.first#id", "p.second#id", "p.third#id"];
    const selectorImg = ["img.a", "img.b", "img.c"]; 

    idIf(id);
    $("#vg").click( function(){

        localStorage.setItem("id", "vg");
    } );
    $("#l").click( function(){

        localStorage.setItem("id", "l");
    } );
    $("#p").click(function(){
        
        localStorage.setItem("id", "p");
    } );

    $(".logout").on("click",function(){
        localStorage.clear();
    });


    $.ajax({
        'url': 'http://localhost:8080/catalogo?id='+id,
        'type':'GET',
        'Access-Control-Allow-Origin': 'http://localhost:8080',
        'Access-Control-Allow-Methods': 'GET',
        'headers': {
            'Accept':'application/json',
            'Content-Type': 'application/json'},
            'responseType':'application/json',
        }).then(function(json){
            $.each(json, function(index, val){
                var name = val.name;
                var itemId = val.id;
                var precio = val.precio;

                $(selectorsI[index]).text(name);
                $(selectorsP[index]).text(precio);
                $(selectorImg[index]).attr("class",itemId);
            })
        });

        
    $("img").click(function (){
        var itemId = $(this).attr("class");
        var imLink = $(this).attr("src");
        localStorage.setItem("imagen", imLink);
        localStorage.setItem("itemId",itemId);
    })

    function idIf(tipo){
        if (tipo === "vg"){
            pasilloVg1();
        } else if(tipo === "l"){
            pasilloL();
        }else if(tipo === "p"){
            pasilloP();
        }
    }
    function pasilloVg1 () {

        var x = ['../HomePage/imagenes/final-fantasy-vii.jpg', '../HomePage/imagenes/jedi.jpg', '../HomePage/imagenes/LastOfUs.jpg'];
        $("img.a").attr("src", x[0]);
        $("img.b").attr("src", x[1]);
        $("img.c").attr("src", x[2]);
    }
    function pasilloP () {

        var x = ['../HomePage/imagenes/trainspotting.jpg', '../HomePage/imagenes/tarantino.jpg', '../HomePage/imagenes/parasite.jpg'];
        $("img.a").attr("src", x[0]);
        $("img.b").attr("src", x[1]);
        $("img.c").attr("src", x[2]);
    }
    function pasilloL () {

        var x = ['../HomePage/imagenes/hp.jpg', '../HomePage/imagenes/junky.jpg', '../HomePage/imagenes/oscar.jpg'];
        $("img.a").attr("src", x[0]);
        $("img.b").attr("src", x[1]);
        $("img.c").attr("src", x[2]);
    }

});