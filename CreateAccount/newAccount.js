
$("document").ready(function(){

    $('#btn').click(function(){
        const email = $('#inputEmail').val();
        const pass =  $('#inputPass').val();
        const name =  $('#Name').val();
        const direccion =  $('#Address').val();
        const tarjeta =  $('#payment').val();

        var val = valData(email,pass, tarjeta);


        if(val === true){


            localStorage.setItem('idUser', name);


            var user = {
                'email': email,
                'pwd': pass,
                'tarjeta': tarjeta, 
                'direccion': direccion,
                'user': name,
            // TODO: cambiar  a request body para mejorar estetica del codigo
            } 

            $.ajax({
                'url': 'http://localhost:8080/newUser?name='+name+'&email='+email+
                '&password='+pass+'&direccion='+direccion+'&tarjeta='+tarjeta,
                'type':'POST',
                'Access-Control-Allow-Origin': 'http://localhost:8080',
                'Access-Control-Allow-Methods': 'POST',
                'headers': {
                    'Accept':'application/json',
                    'Content-Type': 'application/json'},
                'responseType':'application/json',
            });

            // window.location.href= '../HomePage/homepage.html';

        } else{ console.log("algo salio mal")}
        
    });

    function valData(email, pass, tarjeta){
        if(email.includes('@') === false && email.includes('.com') === false) {
            alert('email invalido debe incluir "@" y ".com" ');
            return false;
        } else if(pass.length < 8) {
            alert('Contrasena invalida, minimo 8 caracteres');
            return false;
        } else if(tarjeta.length < 10) {
            alert('tarjeta de credito invalida');
            return false;
        } else {return true;} 
    }

});